import 'package:farmers_market/src/styles/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

abstract class TextStyles {

  static TextStyle get body{
    return GoogleFonts.roboto(textStyle: TextStyle(color:AppColors.darkGrey, fontSize: 16));
  }

  static TextStyle get link{
    return GoogleFonts.roboto(textStyle: TextStyle(color:AppColors.straw, fontSize: 16, fontWeight: FontWeight.bold));
  }

  static TextStyle get suggestion{
    return GoogleFonts.roboto(textStyle: TextStyle(color:AppColors.lightGrey, fontSize: 14));
  } 

    static TextStyle get buttonTextLight{
    return GoogleFonts.roboto(textStyle: TextStyle(color:Colors.white, fontSize: 18, fontWeight: FontWeight.bold));
  }
  
  static TextStyle get buttonTextDark{
    return GoogleFonts.roboto(textStyle: TextStyle(color:Colors.black, fontSize: 18, fontWeight: FontWeight.bold));
  }
}