import 'package:farmers_market/src/styles/base.dart';
import 'package:farmers_market/src/styles/colors.dart';
import 'package:farmers_market/src/styles/texts.dart';
import 'package:flutter/material.dart';

abstract class TextFieldStyles {

  static TextStyle get text => TextStyles.body;

  static TextStyle get placeholder => TextStyles.suggestion;

  static Color get cursorColor => AppColors.darkBlue;

  static Widget iconPrefix(IconData icon) {
    return Padding(
      padding: const EdgeInsets.only(left: 8),
      child: Icon(
        icon,
        size: 30,
        color: AppColors.lightBlue,
      ),
    );
  }

  static BoxDecoration get cupertinoDecoration {
    return BoxDecoration(
          border: Border.all(
        color: AppColors.straw,
        width: BaseStyles.borderWidth,
        
      ),
      borderRadius: BorderRadius.circular(BaseStyles.borderRadius));
  }

  static InputDecoration materialDecoration(String hintText, IconData icon) {
    return InputDecoration(
      prefixIcon: iconPrefix(icon),
      hintText: hintText,
      hintStyle: TextFieldStyles.placeholder,
      border: InputBorder.none,
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color:AppColors.straw, width: BaseStyles.borderWidth),
        borderRadius: BorderRadius.circular(BaseStyles.borderRadius),
      ),
      enabledBorder:  OutlineInputBorder(
        borderSide: BorderSide(color:AppColors.straw, width: BaseStyles.borderWidth),
        borderRadius: BorderRadius.circular(BaseStyles.borderRadius),
      ),
    );
  }
}
