import 'package:farmers_market/src/styles/colors.dart';
import 'package:flutter/material.dart';

abstract class BaseStyles{

  static double get borderRadius  => 20.0;
  
  static double get borderWidth  => 2.0;

  static List<BoxShadow> get boxShadow {
    return[
      BoxShadow(
        color:AppColors.darkGrey.withOpacity(0.5),
        offset: Offset(1.0,2.0),
        blurRadius: 2.0
      )
    ];
  }
}