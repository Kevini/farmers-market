import 'package:flutter/material.dart';

abstract class AppColors {
  static Color get darkGrey => const Color(0xFF4E5B60);

  static Color get lightGrey => const Color(0xFFC8D6EF);

  static Color get darkBlue => const Color(0xFF263A44);

  static Color get lightBlue => const Color(0xFF48A1AF);

  static Color get straw => const Color(0xFFE2A84B);

  static Color get red => const Color(0xFFEe5253);

  static Color get green => const Color(0xFF3B7D02);
}
