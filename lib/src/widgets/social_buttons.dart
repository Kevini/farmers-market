import 'package:farmers_market/src/styles/base.dart';
import 'package:farmers_market/src/styles/buttons.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppSocialButton extends StatelessWidget {

  final SocialType socialType;

  AppSocialButton({@required this.socialType});

  @override
  Widget build(BuildContext context) {
    Color buttonColor;
    Color iconColor;
    IconData icon;

    switch (socialType) {
      case SocialType.Facebook:
      iconColor = Colors.white;
      buttonColor = Color(0xFF3B5998);
      icon = FontAwesomeIcons.facebookF;
        break;
      case SocialType.Google:
      iconColor = Colors.white;
      buttonColor = Color(0xFFDD4B39);
      icon = FontAwesomeIcons.google;
        break;
      default:
      iconColor = Colors.white;
      buttonColor = Color(0xFF3B5998);
      icon = FontAwesomeIcons.facebookF;
      break;
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
      child: Container(
        height: ButtonStyles.buttonHeight,
        width: ButtonStyles.buttonHeight,
        decoration: BoxDecoration(
          color: buttonColor,
          borderRadius: BorderRadius.circular(25.0),
          boxShadow: BaseStyles.boxShadow,
        ),
        child: Icon(icon, color:iconColor),
      ),
    );
  }
}

enum SocialType {Facebook, Google}