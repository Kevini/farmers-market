import 'package:farmers_market/src/styles/base.dart';
import 'package:farmers_market/src/styles/buttons.dart';
import 'package:farmers_market/src/styles/colors.dart';
import 'package:farmers_market/src/styles/texts.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String title;
  final ButtonType buttonType;

  AppButton({@required this.title, this.buttonType});

  @override
  Widget build(BuildContext context) {
    TextStyle fontStyle;
    Color buttonColor;

    switch (buttonType) {
      case ButtonType.Straw:
      fontStyle = TextStyles.buttonTextLight;
      buttonColor = AppColors.straw;
        break;
      case ButtonType.DarkBlue:
      fontStyle = TextStyles.buttonTextLight;
      buttonColor = AppColors.darkBlue;
        break;

        case ButtonType.DarkGrey:
      fontStyle = TextStyles.buttonTextLight;
      buttonColor = AppColors.darkGrey;
        break;
        case ButtonType.LightGrey:
      fontStyle = TextStyles.buttonTextDark;
      buttonColor = AppColors.lightGrey;
        break;  
      default:
      fontStyle = TextStyles.buttonTextLight;
      buttonColor = AppColors.straw;
      break;
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10),
      child: Container(
        height: ButtonStyles.buttonHeight,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: buttonColor,
          borderRadius: BorderRadius.circular(BaseStyles.borderRadius),
          boxShadow: BaseStyles.boxShadow,
        ),
        child: Center(child:Text(title,style:fontStyle)),
      ),
    );
  }
}

enum ButtonType {LightBlue, Straw, LightGrey, DarkGrey, DarkBlue}