import 'package:farmers_market/src/styles/text_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';

class AppTextField extends StatelessWidget {
  final bool hidden;
  final String hintText;
  final IconData materialIcon;
  final IconData cupertinoIcon;
  final TextInputType textInputType;

  AppTextField({
    this.hidden = true,
    @required this.hintText,
    @required this.materialIcon,
    @required this.cupertinoIcon,
    this.textInputType,
  });

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 10.0),
        child: CupertinoTextField(
          keyboardType:
              (textInputType != null) ? textInputType : TextInputType.text,
          padding: EdgeInsets.all(10),
          prefix: TextFieldStyles.iconPrefix(cupertinoIcon),
          placeholder: hintText,
          placeholderStyle: TextFieldStyles.placeholder,
          style: TextFieldStyles.text,
          textAlign: TextAlign.center,
          cursorColor: TextFieldStyles.cursorColor,
          decoration: TextFieldStyles.cupertinoDecoration,
          obscureText: hidden,
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 10.0),
        child: TextField(
          obscureText: hidden,
          keyboardType:
              (textInputType != null) ? textInputType : TextInputType.text,
          cursorColor: TextFieldStyles.cursorColor,
          style: TextFieldStyles.text,
          textAlign: TextAlign.center,
          decoration:
              TextFieldStyles.materialDecoration(hintText, materialIcon),
        ),
      );
    }
  }
}
