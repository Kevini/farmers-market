import 'package:farmers_market/src/styles/texts.dart';
import 'package:farmers_market/src/widgets/button.dart';
import 'package:farmers_market/src/widgets/social_buttons.dart';
import 'package:farmers_market/src/widgets/textfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';

class Signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        child: pageBody(context),
      );
    } else {
      return Scaffold(
        body: pageBody(context),
      );
    }
  }

   Widget pageBody(BuildContext context) {
    return ListView(children: <Widget>[
      SizedBox(height: 100),
      Container(
        height: 200,
        decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage('assets/images/logo.png'))),
      ),
      AppTextField(
          hintText: 'Enter Email Adress',
          materialIcon: Icons.email,
          cupertinoIcon: CupertinoIcons.mail_solid),
      AppTextField(
        hintText: '*******',
        materialIcon: Icons.lock_open,
        cupertinoIcon: CupertinoIcons.padlock_solid,
        hidden: true,
      ),
      AppButton(
        title: 'Signup',
        buttonType: ButtonType.Straw,
      ),
      SizedBox(height: 30),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        AppSocialButton(
          socialType: SocialType.Facebook,
        ),
        AppSocialButton(
          socialType: SocialType.Google,
        ),
      ]),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
        child: RichText(
          text:
              TextSpan(text: 'Already Have an Account ? ', style: TextStyles.body, children: [
            TextSpan(
              text: 'Login',
              style: TextStyles.link,
              recognizer: TapGestureRecognizer().. onTap = () => Navigator.pushNamed(context, '/login')
            ),
          ]),
          textAlign: TextAlign.center,
        ),
      )
    ]);
  }
}
